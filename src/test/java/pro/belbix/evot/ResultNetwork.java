package pro.belbix.evot;

import lombok.Data;
import pro.belbix.evot.protobuf.neuron.NeuralNetworkModel;

import java.time.Instant;

@Data
public class ResultNetwork {
    private final NeuralNetworkModel net;
    private final double result;
    private final Instant created;

    public ResultNetwork(NeuralNetworkModel net, double result) {
        this.net = net;
        this.result = result;
        this.created = Instant.now();
    }
}
