package pro.belbix.evot;

import lombok.Data;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pro.belbix.evot.evolve.NeuralNetworkEvolver;
import pro.belbix.evot.network.InitializerConfig;
import pro.belbix.evot.network.NetworkInitializer;
import pro.belbix.evot.network.SimpleNeuralNetwork;
import pro.belbix.evot.neuron.SimpleNeuron;
import pro.belbix.evot.protobuf.neuron.NeuralNetworkModel;
import pro.belbix.evot.protobuf.neuron.NeuronLayerModel;
import pro.belbix.evot.protobuf.neuron.NeuronModel;
import pro.belbix.evot.protobuf.neuron.Weight;

import java.util.*;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static pro.belbix.evot.utils.RandomUtils.getRandomFromFirstElements;
import static pro.belbix.evot.utils.RandomUtils.randomBinaryInt;

class XorTest {
    private final static boolean FIXED_XOR = false;
    private final static boolean EVOLVE = false;
    private final static boolean DYNAMIC_EVOLVE = false;
    private final static boolean DYNAMIC_EDUCATE = true;
    private final static double EVOLVE_FACTOR = 10;
    private final static int COUNT_OF_XOR_LOOP = 100000;
    private final static int COUNT_OF_EPOCH = 900_000;
    private final static int GET_TOP = 1;
    private boolean printNetwork = false;
    private final List<ResultNetwork> results = new ArrayList<>();
    private SimpleNeuralNetwork network;
    private final Comparator<ResultNetwork> comparator = Comparator.comparing(ResultNetwork::getResult).thenComparing(ResultNetwork::getCreated).reversed();
    private final Map<Integer[], Boolean> xors = new HashMap<>();

    @BeforeEach
    void setUp() {
        if (network != null) {
            network.shutdown();
        }
        if (DYNAMIC_EVOLVE) {
            network = new SimpleNeuralNetwork(new NetworkInitializer(new InitializerConfig()).init(), SimpleNeuron::new, EVOLVE_FACTOR);
        } else {
            network = new SimpleNeuralNetwork(new NetworkInitializer(new InitializerConfig()).init(), SimpleNeuron::new, null);
        }
        if (!FIXED_XOR) {
            generateXors();
        }

    }

    @Test
    void xorMultipleTests() {
        printNetwork = false;
        for (int i = 0; i < 100; i++) {
            ResultNetwork resultNetwork = xorTest();
            assertEquals(100, resultNetwork.getResult());
            results.clear();
            findAndEvolveOneOfTheBestModel();
            if (!FIXED_XOR) {
                generateXors();
            }
        }
    }

    @Test
    void singleXorTest() {
        printNetwork = true;
        xorTest();
    }

    ResultNetwork xorTest() {
        for (int i = 0; i < COUNT_OF_EPOCH; i++) {
//            printNetwork();
            double result;
            if (FIXED_XOR) {
                result = xorFixed();
            } else {
                result = xorFromMap();
            }
            saveResult(result);

            if (result == 100) {
                System.out.println("Complete on " + i);
                break;
            }

            findAndEvolveOneOfTheBestModel();
            if (i % (COUNT_OF_EPOCH / 100) < 1) {
                System.out.println("Top result on iteration " + i + " is : " + results.get(0).getResult());
            }
        }
        System.out.println("Final TOP Result:" + results.get(0).getResult());
        if (printNetwork) {
            System.out.println(results.get(0).getNet().toString());
        }
        return results.get(0);
    }

    private void printNetwork() {
        NeuralNetworkModel net = network.updateNetworkModel();
        NeuronLayerModel layer = net.getLayersList().get(1);
        NeuronModel neuron = layer.getNeurons(0);
        Map.Entry<Integer, Weight> link = neuron.getLinksMap().entrySet().iterator().next();
        System.out.println("l: " + layer.getNeuronsCount()
                + " n:" + link.getKey() + " " + link.getValue());

    }

    private void saveResult(double result) {
        results.add(new ResultNetwork(network.updateNetworkModel(), result));
        results.sort(comparator);

        if (results.size() > GET_TOP) {
            results.remove(results.size() - 1);
        }
    }

    private void findAndEvolveOneOfTheBestModel() {
        NeuralNetworkModel n = getTopNeuralNetwork();
        if (EVOLVE) {
            n = evolveNeuralNetworkModel(n);
        }
        if (network != null) {
            network.shutdown();
        }
        if (DYNAMIC_EVOLVE) {
            network = new SimpleNeuralNetwork(n, SimpleNeuron::new, EVOLVE_FACTOR);
        } else {
            network = new SimpleNeuralNetwork(n, SimpleNeuron::new, null);
        }
    }

    private NeuralNetworkModel evolveNeuralNetworkModel(NeuralNetworkModel n) {
        return new NeuralNetworkEvolver(n, EVOLVE_FACTOR, 1).evolve();
    }

    private NeuralNetworkModel getTopNeuralNetwork() {
        if (results.size() < GET_TOP) {
            return new NetworkInitializer(new InitializerConfig()).init();
        } else {
            return getRandomFromFirstElements(results, GET_TOP).getNet();
        }
    }

    private double xorFixed() {
        int countOfSuccess = 0;
        Result result = new Result();

        result.setExpected(false);
        result.setActual(handle(0, 0));
        if (result.get()) {
            countOfSuccess++;
        }

        result.setExpected(false);
        result.setActual(handle(1, 1));
        if (result.get()) {
            countOfSuccess++;
        }

        result.setExpected(true);
        result.setActual(handle(0, 1));
        if (result.get()) {
            countOfSuccess++;
        }

        result.setExpected(true);
        result.setActual(handle(1, 0));
        if (result.get()) {
            countOfSuccess++;
        }

        return ((double) countOfSuccess / 4) * 100.0;
    }

    private boolean handle(int x, int y) {
        return !network.handle(Arrays.asList(x, y), Double::valueOf).isEmpty();
    }

    private boolean handle(Integer[] arr) {
        return !network.handle(Arrays.asList(arr), Double::valueOf).isEmpty();
    }

    private double xorFromMap() {
        int countOfSuccess = 0;
        for (Map.Entry<Integer[], Boolean> xor : xors.entrySet()) {
            Result result = new Result();
            result.setActual(handle(xor.getKey()));
            result.setExpected(xor.getValue());
            boolean r = result.get();
            if (DYNAMIC_EDUCATE) {
                network.educateNeurons(r, result.actual);
            }
            if (r) {
                countOfSuccess++;
            }
        }
        return ((double) countOfSuccess / COUNT_OF_XOR_LOOP) * 100.0;
    }

//    private double xorRandomLoop() {
//        int countOfSuccess = 0;
//        for (int i = 0; i < COUNT_OF_XOR_LOOP; i++) {
//            Result result = new Result();
//            boolean expectedResult = randomXOR(arr -> result.setActual(handle(arr)));
//            result.setExpected(expectedResult);
//            if (result.get()) {
////                if (DYNAMIC_EDUCATE) network.educateSuccess();
//                countOfSuccess++;
//            } else {
//                if (DYNAMIC_EDUCATE) {
//                    if (result.isActual()) {
//                        network.educateMistakeReduction();
//                    } else {
//                        network.educateMistakeGrowth();
//                    }
//                }
//
//            }
//        }
//        return ((double) countOfSuccess / COUNT_OF_XOR_LOOP) * 100.0;
//    }

    private boolean randomXOR(Consumer<Integer[]> consumer) {
        Integer[] ints = {randomBinaryInt(), randomBinaryInt()};
        consumer.accept(ints);
        boolean expected = false;
        if (ints[0] == 0 && ints[1] == 1) {
            expected = true;
        }
        if (ints[0] == 1 && ints[1] == 0) {
            expected = true;
        }
        return expected;
    }

    private void generateXors() {
        xors.clear();
        for (int i = 0; i < COUNT_OF_XOR_LOOP; i++) {
            Integer[] ints = {randomBinaryInt(), randomBinaryInt()};
            boolean expected = false;
            if (ints[0] == 0 && ints[1] == 1) {
                expected = true;
            }
            if (ints[0] == 1 && ints[1] == 0) {
                expected = true;
            }
            xors.put(ints, expected);
        }
    }

    @Data
    private static class Result {
        private boolean expected;
        private boolean actual;

        public boolean get() {
            return expected == actual;
        }
    }
}