package pro.belbix.evot.data;

import java.io.*;

public class MnistDataReader implements Closeable {
    private final String dataFilePath;
    private final String labelFilePath;
    private int magicNumber;
    private int numberOfItems;
    private int nRows;
    private int nCols;
    private DataInputStream dataInputStream;
    private DataInputStream labelInputStream;

    public MnistDataReader(String dataFilePath, String labelFilePath) throws IOException {
        this.dataFilePath = dataFilePath;
        this.labelFilePath = labelFilePath;
        init();
    }

    private static float normalize(int v) {
        return (float) v / 255f;
    }

    private void init() throws IOException {
        dataInputStream = new DataInputStream(new BufferedInputStream(new FileInputStream(dataFilePath)));
        magicNumber = dataInputStream.readInt();
        numberOfItems = dataInputStream.readInt();
        nRows = dataInputStream.readInt();
        nCols = dataInputStream.readInt();

        System.out.println("magic number is " + magicNumber);
        System.out.println("number of items is " + numberOfItems);
        System.out.println("number of rows is: " + nRows);
        System.out.println("number of cols is: " + nCols);

        labelInputStream = new DataInputStream(new BufferedInputStream(new FileInputStream(labelFilePath)));
        int labelMagicNumber = labelInputStream.readInt();
        int numberOfLabels = labelInputStream.readInt();

        System.out.println("labels magic number is: " + labelMagicNumber);
        System.out.println("number of labels is: " + numberOfLabels);
        assert numberOfItems == numberOfLabels;
    }

    public MnistIntMatrix[] readIntMatrix() throws IOException {
        MnistIntMatrix[] data = new MnistIntMatrix[numberOfItems];
        for (int i = 0; i < numberOfItems; i++) {
            MnistIntMatrix mnistMatrix = new MnistIntMatrix(nRows, nCols);
            mnistMatrix.setLabel(labelInputStream.readUnsignedByte());
            for (int r = 0; r < nRows; r++) {
                for (int c = 0; c < nCols; c++) {
                    mnistMatrix.setValue(r, c, dataInputStream.readUnsignedByte());
                }
            }
            data[i] = mnistMatrix;
        }
        return data;
    }

    public MnistFloatMatrix[] readFloatMatrix() throws IOException {
        MnistFloatMatrix[] data = new MnistFloatMatrix[numberOfItems];
        for (int i = 0; i < numberOfItems; i++) {
            MnistFloatMatrix mnistMatrix = new MnistFloatMatrix(nRows, nCols);
            mnistMatrix.setLabel(labelInputStream.readUnsignedByte());
            for (int r = 0; r < nRows; r++) {
                for (int c = 0; c < nCols; c++) {
                    float vFloat = normalize(dataInputStream.readUnsignedByte());
                    mnistMatrix.setValue(r, c, vFloat);
                }
            }
            data[i] = mnistMatrix;
        }
        return data;
    }

    public MnistFloatList[] readFloatArrays() throws IOException {
        MnistFloatList[] data = new MnistFloatList[numberOfItems];
        for (int i = 0; i < numberOfItems; i++) {
            MnistFloatList mnist = new MnistFloatList();
            mnist.setLabel(labelInputStream.readUnsignedByte());
            for (int r = 0; r < nRows; r++) {
                for (int c = 0; c < nCols; c++) {
                    float vFloat = normalize(dataInputStream.readUnsignedByte());
                    mnist.setValue(r * c, vFloat);
                }
            }
            data[i] = mnist;
        }
        return data;
    }

    public void close() throws IOException {
        dataInputStream.close();
        labelInputStream.close();
    }


}