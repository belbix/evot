package pro.belbix.evot.data;

import java.util.ArrayList;
import java.util.List;

public class MnistFloatList {
    private final List<Float> data = new ArrayList<>();
    private int label;

    public float getValue(int i) {
        return data.get(i);
    }

    public List<Float> getData() {
        return data;
    }

    public void setValue(int i, float value) {
        data.add(value);
    }

    public int getLabel() {
        return label;
    }

    public void setLabel(int label) {
        this.label = label;
    }

    public int getSize() {
        return data.size();
    }

}
