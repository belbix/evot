package pro.belbix.evot;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pro.belbix.evot.data.MnistDataReader;
import pro.belbix.evot.data.MnistFloatList;
import pro.belbix.evot.evolve.NeuralNetworkEvolver;
import pro.belbix.evot.network.InitializerConfig;
import pro.belbix.evot.network.NetworkInitializer;
import pro.belbix.evot.network.SimpleNeuralNetwork;
import pro.belbix.evot.neuron.SimpleNeuron;
import pro.belbix.evot.protobuf.neuron.NeuralNetworkModel;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import static pro.belbix.evot.utils.RandomUtils.getRandomFromFirstElements;

public class MnistTest {
    private static final Logger log = LoggerFactory.getLogger(MnistTest.class);
    private final static String DATA_PATH = "src/test/resources/mnist/train-images.idx3-ubyte";
    private final static String DATA_LABELS_PATH = "src/test/resources/mnist/train-labels.idx1-ubyte";
    private final static String TEST_DATA_PATH = "src/test/resources/mnist/t10k-images.idx3-ubyte";
    private final static String TEST_DATA_LABELS_PATH = "src/test/resources/mnist/t10k-labels.idx1-ubyte";
    private final static boolean DYNAMIC_EVOLVE = false;
    private final static double EVOLVE_FACTOR = 10;
    private final static int COUNT_OF_EPOCH = 900_000;
    private final static int GET_TOP = 10_000;
    private final static int MAX_INPUT = 784;
    private final static int MAX_DATA_COUNT = 10;
    private final static boolean ONLY_ONE = true;
    private final List<ResultNetwork> results = new ArrayList<>();
    private final Comparator<ResultNetwork> comparator = Comparator.comparing(ResultNetwork::getResult).reversed();
    private MnistFloatList[] data;
    private boolean printNetwork = false;
    private SimpleNeuralNetwork network;
    private InitializerConfig conf;

    @BeforeEach
    void setUp() throws IOException {
        conf = new InitializerConfig();
        conf.setCountInitNeurons(MAX_INPUT);
        conf.setCountHiddenLayers(2);
        conf.setCountHiddenNeurons(50);
        conf.setCountResultNeurons(1);
        if (network != null) {
            network.shutdown();
        }
        if (DYNAMIC_EVOLVE) {
            network = new SimpleNeuralNetwork(new NetworkInitializer(conf).init(), SimpleNeuron::new, EVOLVE_FACTOR);
        } else {
            network = new SimpleNeuralNetwork(new NetworkInitializer(conf).init(), SimpleNeuron::new, null);
        }
        load();
    }

    @Test
    void mnistTest() {
        epoch();

    }

    private ResultNetwork epoch() {
        for (int i = 0; i < COUNT_OF_EPOCH; i++) {
            Instant start = Instant.now();
            double result = handleData();
            saveResult(result);
            if (result == 100) {
                log.info("Complete on " + i);
                break;
            }
            initOneOfTheBestModel();
//            if (i % (COUNT_OF_EPOCH / 10) < 1) {
            log.info("Top result on iteration " + i + " is : " + results.get(0).getResult()
                    + " for " + Duration.between(start, Instant.now()).toMillis());
//            }
        }

        log.info("Final TOP Result:" + results.get(0).getResult());
        if (printNetwork) {
            log.info(results.get(0).getNet().toString());
        }
        return results.get(0);
    }

    private double handleData() {
        int countOfSuccess = 0;
        int count = 0;
        for (MnistFloatList m : data) {
            Instant start = Instant.now();
            Set<Integer> r = network.handle(m.getData(), Float::doubleValue);
            boolean success = false;
            if (r.size() == 1 && r.contains(m.getLabel())) {
                countOfSuccess++;
                success = true;
            }
            log.debug(count + " exp: " + m.getLabel() + " r: " + r.toString()
                    + "(" + success + ") for " + Duration.between(start, Instant.now()).toSeconds());
            count++;
            if (count > MAX_DATA_COUNT) {
                break;
            }
        }
        return ((double) countOfSuccess / count) * 100.0;
    }

    private void saveResult(double result) {
        results.add(new ResultNetwork(network.updateNetworkModel().toBuilder().clone().build(), result));
        results.sort(comparator);

        if (results.size() > GET_TOP) {
            results.remove(results.size() - 1);
        }
    }

    private void initOneOfTheBestModel() {
        NeuralNetworkModel n = getTopNeuralNetwork();
        n = evolveNeuralNetworkModel(n);
        if (network != null) {
            network.shutdown();
        }
        if (DYNAMIC_EVOLVE) {
            network = new SimpleNeuralNetwork(n, SimpleNeuron::new, EVOLVE_FACTOR);
        } else {
            network = new SimpleNeuralNetwork(n, SimpleNeuron::new, null);
        }
    }

    private NeuralNetworkModel evolveNeuralNetworkModel(NeuralNetworkModel n) {
        return new NeuralNetworkEvolver(n, EVOLVE_FACTOR, MAX_INPUT - 1).evolve();
    }

    private NeuralNetworkModel getTopNeuralNetwork() {
        if (results.size() < GET_TOP) {
            return new NetworkInitializer(conf).init();
        } else {
            return getRandomFromFirstElements(results, GET_TOP).getNet();
        }
    }

    private void load() throws IOException {
        try (MnistDataReader reader = new MnistDataReader(DATA_PATH, DATA_LABELS_PATH)) {
            data = reader.readFloatArrays();
        }
    }

}
