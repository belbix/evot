package pro.belbix.evot.evolve;

import org.junit.jupiter.api.Test;
import pro.belbix.evot.network.InitializerConfig;
import pro.belbix.evot.network.NetworkInitializer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static pro.belbix.evot.evolve.MutateService.ChanceType.CHANCE_DOUBLE;

class MutateServiceTest {
    private MutateService mutateService = new MutateService(new NetworkInitializer(new InitializerConfig()).init().getLayers(0), 1);

    @Test
    void calcChanceReduceTest() {
        calcChanceAverage(mutateService, 0, true);
        calcChanceAverage(mutateService, 10, true);
        calcChanceAverage(mutateService, 100, true);
        calcChanceAverage(mutateService, 1000, true);
    }

    @Test
    void calcChanceIncreaseTest() {
        calcChanceAverage(mutateService, 0, false);
        calcChanceAverage(mutateService, 10, false);
        calcChanceAverage(mutateService, 100, false);
        calcChanceAverage(mutateService, 1000, false);
    }

    //    @Test
    void MutateChangeTest() {
        assertEquals(0.1, mutateService.getMutateChange(0));
        assertEquals(0.1, mutateService.getMutateChange(1));
        assertEquals(0.06213349345596119, mutateService.getMutateChange(5));
        assertEquals(0.04342944819032518, mutateService.getMutateChange(10));
        assertEquals(0.033380820069533405, mutateService.getMutateChange(20));
        assertEquals(0.025562221863533147, mutateService.getMutateChange(50));
        assertEquals(0.02171472409516259, mutateService.getMutateChange(100));
        assertEquals(0.014476482730108396, mutateService.getMutateChange(1000));
        assertEquals(0, mutateService.getMutateChange(mutateService.getNeuronMutateFacrtorMax()));
    }

    private double calcChanceAverage(MutateService mutateService, int count, boolean reduce) {
        int loops = 10;
        double sum = 0;
        for (int i = 0; i < loops; i++) {
            sum += mutateService.calcChance(count, CHANCE_DOUBLE, reduce);
        }
        double avg = sum / loops;
        System.out.println("Average result for " + count + ":" + avg);
        return avg;
    }
}