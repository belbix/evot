package pro.belbix.evot.neuron;

import lombok.AllArgsConstructor;
import lombok.Data;
import pro.belbix.evot.evolve.MutateService;

@Data
@AllArgsConstructor
public class EducationState {
    private final MutateService mutateService;
    private final boolean successResult;
    private final boolean resultWasActive;
    private final int iterate;
}
