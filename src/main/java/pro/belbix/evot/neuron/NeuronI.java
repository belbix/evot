package pro.belbix.evot.neuron;

import pro.belbix.evot.utils.DoubleWithCount;

import java.util.Map;

public interface NeuronI {
    Map<Integer, Map<Integer, DoubleWithCount>> handleSignal(double signal);

    void clear();

    int getId();

    int getIndexOfInput();

    long getActivatedCount();

    Map<Integer, Map<Integer, DoubleWithCount>> getLinks();

    void educate(EducationState h);

    double getMutateFactor();
}
