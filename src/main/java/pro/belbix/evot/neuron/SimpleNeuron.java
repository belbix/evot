package pro.belbix.evot.neuron;

import com.google.common.util.concurrent.AtomicDouble;
import lombok.ToString;
import pro.belbix.evot.evolve.MutateService;
import pro.belbix.evot.protobuf.neuron.DoubleWithCounter;
import pro.belbix.evot.protobuf.neuron.NeuronModel;
import pro.belbix.evot.protobuf.neuron.Weight;
import pro.belbix.evot.utils.DoubleWithCount;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

@ToString
public class SimpleNeuron implements NeuronI {
    private final int id;
    private final int layerId;
    private final int indexOfInput;
    private final double threshold;
    private final boolean positive;
    private final Map<Integer, Map<Integer, DoubleWithCount>> links;
    private final AtomicDouble state = new AtomicDouble(0);
    private final AtomicBoolean activated = new AtomicBoolean(false);
    private double mutateFactor;
    private long activatedCount = 0;

    public SimpleNeuron(NeuronModel neuronModel, int layerId) {
        this.id = neuronModel.getId();
        this.layerId = layerId;
        this.indexOfInput = neuronModel.getIndexOfInput();
        this.threshold = neuronModel.getThreshold();
        this.positive = neuronModel.getPositive();
        this.activatedCount = neuronModel.getActivatedCount();
        this.mutateFactor = neuronModel.getMutateFactor();
        this.links = initLinks(neuronModel);
    }

    private Map<Integer, Map<Integer, DoubleWithCount>> initLinks(NeuronModel neuronModel) {
        Map<Integer, Map<Integer, DoubleWithCount>> links = new ConcurrentHashMap<>();
        for (Map.Entry<Integer, Weight> link : neuronModel.getLinksMap().entrySet()) {
            Map<Integer, DoubleWithCount> weights = new ConcurrentHashMap<>();
            links.put(link.getKey(), weights);
            for (Map.Entry<Integer, DoubleWithCounter> weight : link.getValue().getWeightsMap().entrySet()) {
                DoubleWithCounter d = weight.getValue();
                weights.put(weight.getKey(), new DoubleWithCount(d.getValue(), d.getCount(), d.getPositive()));
            }
        }
        return links;
    }

    @Override
    public Map<Integer, Map<Integer, DoubleWithCount>> handleSignal(double signal) {
        if (activated.get()) {
            return null;
        }
        double updatedState = state.addAndGet(signal);
        if (positive) {
            if (updatedState >= threshold) {
                if (!activated.compareAndSet(false, true)) {
                    return null;
                }
            }
        } else {
            if (updatedState <= threshold) {
                if (!activated.compareAndSet(false, true)) {
                    return null;
                }
            }
        }
        if (activated.get()) {
            activatedCount++;
            return links;
        }
        return null;
    }

    @Override
    public void clear() {
        state.set(0);
        activated.set(false);
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public int getIndexOfInput() {
        return indexOfInput;
    }

    @Override
    public long getActivatedCount() {
        return activatedCount;
    }

    @Override
    public Map<Integer, Map<Integer, DoubleWithCount>> getLinks() {
        return links;
    }

    public double getMutateFactor() {
        return mutateFactor;
    }

    @Override
    public void educate(EducationState h) {
        MutateService mutateService = h.getMutateService();
        boolean successResult = h.isSuccessResult();
        boolean resultWasActive = h.isResultWasActive();


        if (successResult) {
            if (resultWasActive) {
                changeMutate(1, mutateService);
            }
        } else {
            changeMutate(-1, mutateService);
            forEachLink(d -> {
                changeDouble(d, mutateService, !d.isActivated());
            });
        }

    }

    private void changeMutate(double value, MutateService mutateService) {
        if (mutateFactor > mutateService.getNeuronMutateFacrtorMax()
                || mutateFactor <= 0) {
            return;
        }
        mutateFactor += value;
    }

    private void changeDouble(DoubleWithCount d, MutateService mutateService, boolean increase) {
        double change = mutateService.getMutateChange(mutateFactor);
        if (d.isPositive()) {
            if (d.getValue() > mutateService.getMutateDoubleMax()) {
                return;
            }
        } else {
            if (d.getValue() < -mutateService.getMutateDoubleMax()) {
                return;
            }
            change = -change;
        }
        if (increase) {
            d.changeValue(change);
        } else {
            d.changeValue(-change);
        }
    }

    private void forEachLink(Consumer<DoubleWithCount> c) {
        for (Map<Integer, DoubleWithCount> linkLayer : links.values()) {
            for (DoubleWithCount d : linkLayer.values()) {
                c.accept(d);
            }
        }
    }
}
