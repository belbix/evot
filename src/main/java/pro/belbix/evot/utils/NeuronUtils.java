package pro.belbix.evot.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;
import pro.belbix.evot.protobuf.neuron.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static pro.belbix.evot.utils.RandomUtils.random;
import static pro.belbix.evot.utils.RandomUtils.randomWithNegative;

public class NeuronUtils {

    public static NeuronLayerModel findLayer(int id, NeuralNetworkModel net) {
        for (NeuronLayerModel layerModel : net.getLayersList()) {
            if (layerModel.getId() == id) {
                return layerModel;
            }
        }
        return null;
    }

    public static NeuronModel findNeuronInLayer(NeuronLayerModel layerModel, int id) {
        for (NeuronModel n : layerModel.getNeuronsList()) {
            if (n.getId() == id) {
                return n;
            }
        }
        throw new IllegalStateException("Not found neuron with id " + id);
    }

    public static double defaultWeight() {
//        return 1;
        return randomWithNegative();
    }

    public static double defaultInitThreshold() {
//        return 0;
        return random() + 0.01;
//        return random();
    }

    public static double defaultThreshold() {
//        return 0;
        return defaultInitThreshold();
    }

    public static Map<Integer, Weight> updateLinks(Map<Integer, Weight> modelLinks,
                                                   Function<NeuronPosition, DoubleWithCounter> doubleProducer) {
        Map<Integer, Weight> newLinks = new HashMap<>();
        for (Map.Entry<Integer, Weight> link : modelLinks.entrySet()) {
            int layerId = link.getKey();
            Weight.Builder newWeight = Weight.newBuilder();
            for (Map.Entry<Integer, DoubleWithCounter> weights : link.getValue().getWeightsMap().entrySet()) {
                int neuronId = weights.getKey();
                DoubleWithCounter dbl = doubleProducer.apply(new NeuronPosition(layerId, neuronId));
                if (dbl == null) {
                    continue;
                }
                newWeight.putWeights(neuronId, dbl);
            }

            newLinks.put(layerId, newWeight.build());
        }
        return newLinks;
    }

    public static List<Float> normalizeByte(List<Byte> bytes) {
        List<Float> data = new ArrayList<>();
        for (Byte aByte : bytes) {
            Float f = aByte.floatValue() / 255;
            data.add(f);
        }
        return data;
    }

    @Data
    @AllArgsConstructor
    @ToString
    public static class NeuronPosition {
        private int layerId;
        private int neuronId;
    }
}
