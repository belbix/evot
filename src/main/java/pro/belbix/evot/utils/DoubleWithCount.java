package pro.belbix.evot.utils;

import com.google.common.util.concurrent.AtomicDouble;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import pro.belbix.evot.evolve.MutateService;

import java.util.concurrent.atomic.AtomicLong;

import static pro.belbix.evot.evolve.MutateService.ChanceType.CHANCE_DOUBLE;
import static pro.belbix.evot.evolve.MutateService.isChance;

@EqualsAndHashCode
@ToString
public class DoubleWithCount {
    private final AtomicDouble value;
    private final AtomicLong count;
    private final boolean positive;
    private boolean activated = false;

    public DoubleWithCount(double value, long count, boolean positive) {
        this.value = new AtomicDouble(value);
        this.count = new AtomicLong(count);
        this.positive = positive;
    }

    public void increaseCount() {
        count.incrementAndGet();
    }

    public void tryToChangeValue(double value, MutateService mutateService) {
        if (!isChance(mutateService.calcChance(count.get(), CHANCE_DOUBLE, false))) {
            return;
        }
        this.value.addAndGet(value);
    }

    public void changeValue(double value) {
        this.value.addAndGet(value);
    }

    public double getValue() {
        return value.get();
    }

    public long getCount() {
        return count.get();
    }

    public boolean isPositive() {
        return positive;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }
}
