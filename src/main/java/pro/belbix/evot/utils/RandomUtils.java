package pro.belbix.evot.utils;

import org.apache.commons.rng.sampling.distribution.ZigguratNormalizedGaussianSampler;
import org.apache.commons.rng.simple.RandomSource;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class RandomUtils {

    public static <T> T getRandomElement(Set<T> set) {
        List<T> list = new ArrayList<>(set);
        return getRandomElement(list);
    }

    public static <T> T getRandomElement(List<T> list) {
        if (list.size() == 1) {
            return list.get(0);
        }
        return list.get(ThreadLocalRandom.current().nextInt(0, list.size()));
    }

    public static double normalDistributionRandom() {
        return ZigguratNormalizedGaussianSampler.of(RandomSource.create(RandomSource.MT_64)).sample();
    }

    public static double random() {
        return ThreadLocalRandom.current().nextDouble();
    }

    public static int nextInt(int from, int to) {
        if (from == to) {
            return to;
        } else {
            return ThreadLocalRandom.current().nextInt(from, to);
        }
    }

    public static double randomWithNegative() {
        return ThreadLocalRandom.current().nextDouble(-1, 1);
    }

    public static int randomBinaryInt() {
        return ThreadLocalRandom.current().nextInt(0, 2);
    }

    public static <K, V> V getRandomFromFirstEntries(Map<K, V> source, int max) {
        List<V> v = new ArrayList<>(getFirstEntries(source, max).values());
        return getRandomElement(v);
    }

    public static <T> T getRandomFromFirstElements(List<T> source, int max) {
        return getRandomElement(getFirstElements(source, max));
    }

    public static <T> T getRandomFromFirstElements(Set<T> source, int max) {
        List<T> v = new ArrayList<>(getFirstElements(source, max));
        return getRandomElement(v);
    }

    public static <K, V> Map<K, V> getFirstEntries(Map<K, V> source, int max) {
        int count = 0;
        Map<K, V> target = new HashMap<>();
        for (Map.Entry<K, V> entry : source.entrySet()) {
            if (count >= max) break;
            target.put(entry.getKey(), entry.getValue());
            count++;
        }
        return target;
    }

    public static <K> Set<K> getFirstElements(Set<K> source, int max) {
        int count = 0;
        Set<K> target = new HashSet<>();
        for (K el : source) {
            if (count >= max) break;
            target.add(el);
            count++;
        }
        return target;
    }

    public static <K> List<K> getFirstElements(List<K> source, int max) {
        int count = 0;
        List<K> target = new ArrayList<>();
        for (K el : source) {
            if (count >= max) break;
            target.add(el);
            count++;
        }
        return target;
    }

}
