package pro.belbix.evot.network;

import pro.belbix.evot.neuron.NeuronI;
import pro.belbix.evot.utils.DoubleWithCount;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class NeuronHandler implements Runnable {
    private final NeuronI neuron;
    private final Map<Integer, Map<Integer, NeuronI>> layers;
    private final Set<Integer> results;
    private final int indexOfInput;
    private final LinkedBlockingQueue<Double> values = new LinkedBlockingQueue<>();
    private final Latch countDownLatch;
    private boolean run = true;

    public NeuronHandler(NeuronI neuron, Map<Integer, Map<Integer, NeuronI>> layers, Set<Integer> results, Latch countDownLatch) {
        this.neuron = neuron;
        this.layers = layers;
        this.results = results;
        this.countDownLatch = countDownLatch;
        this.indexOfInput = neuron.getIndexOfInput();
    }

    private static void updateLinksStatus(NeuronI neuron, int linkLayerId, int sourceNeuronId, boolean activated) {
        if (neuron == null) {
            return;
        }
        try {
            DoubleWithCount doubleWithCount = neuron.getLinks().get(linkLayerId).get(sourceNeuronId);
            if (activated) {
                doubleWithCount.increaseCount();
            }
            doubleWithCount.setActivated(activated);
        } catch (NullPointerException e) {
            throw new IllegalStateException("Can't find link " + linkLayerId + ":" + sourceNeuronId + " in " + neuron);
        }

    }

    @Override
    public void run() {
        while (run) {
            try {
                Double v = values.poll(100, TimeUnit.MILLISECONDS);
                if (v == null) {
                    continue;
                }
                handleNeuron(v, neuron, 0, null);
                countDownLatch.countDown();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void addValue(double value) {
        values.add(value);
    }

    private void handleNeuron(double value, NeuronI neuron, int neuronLayerId, NeuronI whoSent) {
        Map<Integer, Map<Integer, DoubleWithCount>> answer = neuron.handleSignal(value);
        boolean activated = answer != null;
        updateLinksStatus(whoSent, neuronLayerId, neuron.getId(), activated);
        if (!activated) {
            return;
        }

        for (Map.Entry<Integer, Map<Integer, DoubleWithCount>> link : answer.entrySet()) {
            Map<Integer, NeuronI> nextLayer = layers.get(link.getKey());
            if (nextLayer == null) {
                throw new IllegalStateException("Layer not found " + link.getKey() + " neuron: " + neuron);
            }
            for (Map.Entry<Integer, DoubleWithCount> weight : link.getValue().entrySet()) {
                NeuronI nextNeuron = nextLayer.get(weight.getKey());
                if (nextNeuron == null) {
                    deleteDeathLink(weight.getKey(), link.getKey());
                    continue;
                }

                if (link.getKey() == -1) {
                    handleResult(nextNeuron, weight.getValue().getValue(), neuron);
                    continue;
                }
                handleNeuron(weight.getValue().getValue(), nextNeuron, link.getKey(), neuron);
            }
        }
    }

    private void deleteDeathLink(int id, int layerId) {
        neuron.getLinks().get(layerId).remove(id);
    }

    private void handleResult(NeuronI resultNeuron, double signal, NeuronI whoSent) {
        Map<Integer, Map<Integer, DoubleWithCount>> answer = resultNeuron.handleSignal(signal);
        boolean activated = answer != null;
        updateLinksStatus(whoSent, -1, resultNeuron.getId(), activated);
        if (activated) {
            results.add(resultNeuron.getId());
        }
    }

    public int getIndexOfInput() {
        return indexOfInput;
    }

    public void shutdown() {
        run = false;
    }
}
