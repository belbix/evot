package pro.belbix.evot.network;

import pro.belbix.evot.protobuf.neuron.*;

import static pro.belbix.evot.utils.NeuronUtils.*;

public class NetworkInitializer {
    private final InitializerConfig conf;
    private final NeuralNetworkModel.Builder net;

    public NetworkInitializer(InitializerConfig conf) {
        this.conf = conf;
        this.net = NeuralNetworkModel.newBuilder();
    }

    private static void initDefaultNeuron(NeuronModel.Builder neuron) {
        neuron.setPositive(true)
                .setEpochCount(0)
                .setMutateFactor(0);
    }

    public NeuralNetworkModel init() {
        putLayer(initInputLayer());
        int countOfHidden = 1;
        while (countOfHidden < conf.getCountHiddenLayers() + 1) {
            putLayer(initHiddenLayer(countOfHidden));
            countOfHidden++;
        }
        putLayer(initResultLayer());
        return net.build();
    }

    private NeuronLayerModel initInputLayer() {
        NeuronLayerModel.Builder layer = NeuronLayerModel.newBuilder();
        layer.setId(0);
        initDefaultMutateChances(layer);
        int countOfNeurons = 0;
        while (countOfNeurons < conf.getCountInitNeurons()) {
            layer.addNeurons(initInputNeurons(countOfNeurons));
            countOfNeurons++;
        }
        return layer.build();
    }

    private NeuronLayerModel initHiddenLayer(int layerId) {
        NeuronLayerModel.Builder layer = NeuronLayerModel.newBuilder();
        layer.setId(layerId);
        initDefaultMutateChances(layer);
        int countOfNeurons = 0;
        while (countOfNeurons < conf.getCountHiddenNeurons()) {
            layer.addNeurons(initHiddenNeurons(countOfNeurons, layerId));
            countOfNeurons++;
        }
        return layer.build();
    }

    private NeuronLayerModel initResultLayer() {
        NeuronLayerModel.Builder layer = NeuronLayerModel.newBuilder();
        layer.setId(-1);
        initDefaultMutateChances(layer);
        int countOfNeurons = 0;
        while (countOfNeurons < conf.getCountResultNeurons()) {
            layer.addNeurons(initResultNeurons(countOfNeurons));
            countOfNeurons++;
        }
        return layer.build();
    }

    private NeuronModel initInputNeurons(int id) {
        NeuronModel.Builder neuron = NeuronModel.newBuilder();
        neuron.setId(id)
                .setIndexOfInput(id)
                .setThreshold(defaultInitThreshold());
        Weight.Builder weights = Weight.newBuilder();
        for (int i = 0; i < conf.getCountHiddenNeurons(); i++) {
            weights.putWeights(i, DoubleWithCounter.newBuilder().setValue(defaultWeight())
                    .setPositive(true).build()); //TODO make class for linking
        }
        neuron.putLinks(1, weights.build());
        initDefaultNeuron(neuron);
        return neuron.build();
    }

    private NeuronModel initHiddenNeurons(int id, int layerId) {
        NeuronModel.Builder neuron = NeuronModel.newBuilder();
        neuron.setId(id)
                .setThreshold(defaultThreshold());
        Weight.Builder weights = Weight.newBuilder();
        if (layerId == conf.getCountHiddenLayers()) {
            for (int i = 0; i < conf.getCountResultNeurons(); i++) {
                weights.putWeights(i, DoubleWithCounter.newBuilder().setValue(defaultWeight())
                        .setPositive(true).build());
            }
            neuron.putLinks(-1, weights.build());
        } else {
            for (int i = 0; i < conf.getCountHiddenNeurons(); i++) {
                weights.putWeights(i, DoubleWithCounter.newBuilder().setValue(defaultWeight())
                        .setPositive(true).build());
            }
            neuron.putLinks(layerId + 1, weights.build());
        }

        initDefaultNeuron(neuron);
        return neuron.build();
    }

    private void putLayer(NeuronLayerModel layer) {
        net.addLayers(layer);
    }

    private NeuronModel initResultNeurons(int id) {
        NeuronModel.Builder neuron = NeuronModel.newBuilder();
        neuron.setId(id).setThreshold(1);
        initDefaultNeuron(neuron);
        return neuron.build();
    }

    private void initDefaultMutateChances(NeuronLayerModel.Builder layer) {
        layer.setMutateDoubleMax(100)
                .setMutateDoubleBase(0.1)
                .setBaseChanceDouble(50)
                .setBaseChanceLink(10)
                .setBaseChanceNeuron(5)
                .setBaseChanceHidden(1)
                .setCountBase(100)
                .setCountMin(1);
    }

}
