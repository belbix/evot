package pro.belbix.evot.network;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class InitializerConfig {
    private int countInitNeurons = 2;
    private int countHiddenNeurons = 3;
    private int countHiddenLayers = 1;
    private int countResultNeurons = 1;
}
