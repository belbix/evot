package pro.belbix.evot.network;

import pro.belbix.evot.evolve.MutateService;
import pro.belbix.evot.neuron.EducationState;
import pro.belbix.evot.neuron.NeuronI;
import pro.belbix.evot.protobuf.neuron.*;
import pro.belbix.evot.utils.DoubleWithCount;
import pro.belbix.evot.utils.NeuronUtils;

import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.function.BiFunction;
import java.util.function.Function;

public class SimpleNeuralNetwork {
    private final BiFunction<NeuronModel, Integer, NeuronI> neuronProducer;
    private NeuralNetworkModel neuralNetworkModel;
    private final boolean dynamicEducation;
    private Map<Integer, Map<Integer, NeuronI>> layers;
    private final Set<Integer> results = new ConcurrentSkipListSet<>();
    private final double educationFactor;
    private Map<Integer, MutateService> mutateServiceMap;
    private final Map<NeuronHandler, Thread> neuronHandlers = new HashMap<>();
    private Latch countDownLatch;

    public SimpleNeuralNetwork(NeuralNetworkModel model,
                               BiFunction<NeuronModel, Integer, NeuronI> neuronProducer,
                               Double educationFactor) {
        this.neuralNetworkModel = model;
        this.neuronProducer = neuronProducer;
        initLayers();
        initNeuronHandlers();
        initMutateServices();
        if (educationFactor != null) {
            this.dynamicEducation = true;
            this.educationFactor = educationFactor;
        } else {
            this.dynamicEducation = false;
            this.educationFactor = 0;
        }
    }

    private void initLayers() {
        Map<Integer, Map<Integer, NeuronI>> tmpMap = new HashMap<>();
        for (NeuronLayerModel layer : neuralNetworkModel.getLayersList()) {
            Map<Integer, NeuronI> tmpInnerMap = new HashMap<>();
            for (NeuronModel n : layer.getNeuronsList()) {
                tmpInnerMap.put(n.getId(), neuronProducer.apply(n, layer.getId()));
            }
            tmpMap.put(layer.getId(), Collections.unmodifiableMap(tmpInnerMap));
        }
        this.layers = Collections.unmodifiableMap(tmpMap);
    }

    private void initNeuronHandlers() {
        countDownLatch = new Latch(layers.get(0).size());
        for (NeuronI neuronI : layers.get(0).values()) {
            NeuronHandler neuronHandler = new NeuronHandler(neuronI, layers, results, countDownLatch);
            Thread thread = new Thread(neuronHandler);
            neuronHandlers.put(neuronHandler, thread);
            thread.setName("InitNeuron" + neuronI.getId());
            thread.start();
        }
    }

    private void initMutateServices() {
        mutateServiceMap = new HashMap<>();
        for (NeuronLayerModel layer : neuralNetworkModel.getLayersList()) {
            mutateServiceMap.put(layer.getId(), new MutateService(layer, educationFactor));
        }
    }

    public <T> Set<Integer> handle(List<T> objs, Function<T, Double> valueProducer) {
        clear();
        for (Map.Entry<NeuronHandler, Thread> handler : neuronHandlers.entrySet()) {
            T obj;
            try {
                obj = objs.get(handler.getKey().getIndexOfInput());
            } catch (IndexOutOfBoundsException e) {
                continue;
            }
            double value = valueProducer.apply(obj);
            handler.getKey().addValue(value);
        }

        waitUntilAllNeuronsEnd();

//        if (dynamicEducation) {
//            educateNeurons();
//        }
        return results;
    }

    private void waitUntilAllNeuronsEnd() {
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        countDownLatch.reset();
    }

    private void clear() {
        results.clear();
        layers.values().forEach(m -> m.values().forEach(NeuronI::clear));
    }

    public void shutdown() {
        for (NeuronHandler handler : neuronHandlers.keySet()) {
            handler.shutdown();
        }
    }

    public NeuralNetworkModel updateNetworkModel() {
        NeuralNetworkModel.Builder builder = NeuralNetworkModel.newBuilder();
        for (NeuronLayerModel layer : neuralNetworkModel.getLayersList()) {
            NeuronLayerModel updatedLayer = updateLayer(layer, this.layers.get(layer.getId()));
            builder.addLayers(updatedLayer);
        }
        neuralNetworkModel = builder.build();
        initLayers();
        initMutateServices();
        initNeuronHandlers();
        return neuralNetworkModel;
    }

    private NeuronLayerModel updateLayer(NeuronLayerModel layer, Map<Integer, NeuronI> neurons) {
        if (layer.getId() < 0) {
            return layer;
        }
        NeuronLayerModel.Builder builder = layer.toBuilder();
        builder.clearNeurons();
        for (NeuronModel neuronModel : layer.getNeuronsList()) {
            NeuronI neuronI = neurons.get(neuronModel.getId());
            if (neuronI == null) {
                throw new IllegalStateException();
            }
            NeuronModel.Builder neuronBuilder = neuronModel.toBuilder();

            Map<Integer, Weight> newLinks = NeuronUtils.updateLinks(neuronBuilder.getLinksMap(), p -> {
                DoubleWithCount d;
                try {
                    d = neuronI.getLinks().get(p.getLayerId()).get(p.getNeuronId());
                    if (d == null) {
                        return null;
                    }
                    return DoubleWithCounter.newBuilder()
                            .setValue(d.getValue())
                            .setCount(d.getCount())
                            .setPositive(d.isPositive())
                            .build();
                } catch (NullPointerException e) {
                    throw new IllegalStateException("Not found " + p);
                }
            });

            builder.addNeurons(
                    neuronBuilder
                            .setActivatedCount(neuronI.getActivatedCount())
                            .setMutateFactor(neuronI.getMutateFactor())
                            .clearLinks()
                            .putAllLinks(newLinks)
            );
        }
        return builder.build();
    }

    public void educateNeurons(boolean successResult, boolean resultWasActive) {
        for (Map.Entry<Integer, Map<Integer, NeuronI>> layer : layers.entrySet()) {
            if (layer.getKey() < 0) {
                continue;
            }
            MutateService mutateService = mutateServiceMap.get(layer.getKey());
            for (Map.Entry<Integer, NeuronI> neuronEntry : layer.getValue().entrySet()) {
                neuronEntry.getValue().educate(new EducationState(
                        mutateService,
                        successResult,
                        resultWasActive,
                        0
                ));
            }
        }
    }
}
