package pro.belbix.evot.evolve;

import pro.belbix.evot.protobuf.neuron.DoubleWithCounter;
import pro.belbix.evot.protobuf.neuron.NeuronLayerModel;
import pro.belbix.evot.protobuf.neuron.NeuronModel;
import pro.belbix.evot.protobuf.neuron.Weight;

import java.util.List;
import java.util.stream.Collectors;

import static pro.belbix.evot.evolve.MutateService.ChanceType.CHANCE_LINK;
import static pro.belbix.evot.evolve.MutateService.ChanceType.FIFTY_CHANCE;
import static pro.belbix.evot.evolve.MutateService.isChance;
import static pro.belbix.evot.utils.NeuronUtils.*;
import static pro.belbix.evot.utils.RandomUtils.getRandomElement;
import static pro.belbix.evot.utils.RandomUtils.nextInt;

public class NeuronProducer {

    public static NeuronModel cloneRandomNeuron(List<NeuronModel> actualNeurons, MutateService mutateService, int maxInputId) {
        NeuronModel parent = getRandomElement(actualNeurons);
        NeuronModel.Builder builder = parent.toBuilder().clone();

        int maxId = findMaxId(actualNeurons);
        int currentMaxInputId = findMaxInputId(actualNeurons);

        if (currentMaxInputId != Integer.MIN_VALUE) {
            builder.setIndexOfInput(randomInputId(currentMaxInputId, maxInputId, mutateService));
        }

        boolean isPositive = isChance(mutateService.calcChance(0, FIFTY_CHANCE, true));

        if (parent.getPositive() == isPositive) {
            return builder
                    .setPositive(isPositive)
                    .setId(maxId + 1)
                    .setEpochCount(0)
                    .build();
        } else {
            return builder
                    .setPositive(isPositive)
                    .setId(maxId + 1)
                    .setEpochCount(0)
                    .setThreshold(0)
                    .setMutateFactor(0)
                    .build();
        }
    }

    public static NeuronModel createNeuron(List<NeuronModel> actualNeurons,
                                           NeuronLayerModel nextLayer,
                                           MutateService mutateService,
                                           int maxInputId) {
        NeuronModel.Builder builder = NeuronModel.newBuilder();
        NeuronModel firstNeuron = null;
        if (!actualNeurons.isEmpty()) {
            firstNeuron = actualNeurons.iterator().next();
            builder.mergeFrom(firstNeuron);
        } else {
            builder.setThreshold(defaultThreshold());
        }

        int currentMaxInputId = findMaxInputId(actualNeurons);
        if (currentMaxInputId != Integer.MIN_VALUE) {
            builder.setIndexOfInput(randomInputId(currentMaxInputId, maxInputId, mutateService));
        }

        if (nextLayer != null
                && firstNeuron != null
                && firstNeuron.getLinksCount() > 0) {
            Integer nextId = getRandomElement(
                    nextLayer.getNeuronsList().stream()
                            .map(NeuronModel::getId)
                            .collect(Collectors.toList())
            );
            NeuronModel nextNeuron = findNeuronInLayer(nextLayer, nextId);
            builder.putLinks(nextLayer.getId(),
                    Weight.newBuilder().putWeights(nextId,
                            DoubleWithCounter.newBuilder().setValue(defaultWeight())
                                    .setPositive(nextNeuron.getPositive()).build()
                    ).build()
            );
        }

        return builder.setId(findMaxId(actualNeurons) + 1)
                .setPositive(isChance(mutateService.calcChance(0, FIFTY_CHANCE, true)))
                .setEpochCount(0)
                .setMutateFactor(0)
                .build();
    }

    private static int randomInputId(int currentMaxInputId, int maxInputId, MutateService mutateService) {

        int indexOfInput;
        //try to add next index of input
        if (isChance(mutateService.calcChance(0, CHANCE_LINK, true))
                && currentMaxInputId < maxInputId) {
            indexOfInput = currentMaxInputId + 1;
        } else {
            indexOfInput = nextInt(0, currentMaxInputId);
        }
        return indexOfInput;
    }

    private static int findMaxId(List<NeuronModel> neurons) {
        int maxId = 0;
        for (NeuronModel n : neurons) {
            if (n.getId() > maxId) {
                maxId = n.getId();
            }
        }
        return maxId;
    }

    private static int findMaxInputId(List<NeuronModel> neurons) {
        int maxInputId = Integer.MIN_VALUE;
        for (NeuronModel n : neurons) {
            if (n.hasIndexOfInput() && n.getIndexOfInput() > maxInputId) {
                maxInputId = n.getIndexOfInput();
            }
        }
        return maxInputId;
    }

}
