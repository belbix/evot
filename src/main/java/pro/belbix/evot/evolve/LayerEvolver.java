package pro.belbix.evot.evolve;

import pro.belbix.evot.protobuf.neuron.NeuralNetworkModel;
import pro.belbix.evot.protobuf.neuron.NeuronLayerModel;
import pro.belbix.evot.protobuf.neuron.NeuronModel;

import static pro.belbix.evot.evolve.MutateService.ChanceType.CHANCE_NEURON_CREATE;
import static pro.belbix.evot.evolve.MutateService.ChanceType.CHANCE_NEURON_DEATH;
import static pro.belbix.evot.evolve.MutateService.isChance;
import static pro.belbix.evot.utils.NeuronUtils.findLayer;

public class LayerEvolver {
    private final static boolean TRACE = false;
    private final NeuralNetworkModel net;
    private final int layerId;
    private final double evolveFactor;
    private final int maxInput;

    public LayerEvolver(NeuralNetworkModel net, int layerId, double evolveFactor, int maxInput) {
        this.net = net;
        this.layerId = layerId;
        this.evolveFactor = evolveFactor;
        this.maxInput = maxInput;
    }

    public NeuronLayerModel evolve() {
        NeuronLayerModel layer = findLayer(layerId, net);
        if (layer == null) {
            throw new IllegalStateException("Layer not found");
        }
        MutateService mutateService = new MutateService(layer, evolveFactor);

        NeuronLayerModel.Builder builder = NeuronLayerModel.newBuilder();
        builder.setId(layer.getId());
        mutateService.mutateLayersChance(builder, layer);

        for (NeuronModel n : layer.getNeuronsList()) {
            if (builder.getNeuronsCount() > 0) {
                boolean isKillNeuron = isChance(mutateService.calcChance(n.getActivatedCount(), CHANCE_NEURON_DEATH, true));
                if (isKillNeuron) {
                    if (TRACE) System.out.println("Kill " + n);
                    continue;
                }
            }

            builder.addNeurons(new NeuronEvolver(n, layerId, net, mutateService, true).evolve());
        }

        boolean isCreateNeuron = isChance(mutateService.calcChance(0, CHANCE_NEURON_CREATE, true));
        if (isCreateNeuron) {
            if (layer.getNeuronsList().isEmpty()) {
                builder.addNeurons(NeuronProducer.createNeuron(layer.getNeuronsList(), findLayerConnection(), mutateService, maxInput));
            } else {
                builder.addNeurons(NeuronProducer.cloneRandomNeuron(layer.getNeuronsList(), mutateService, maxInput));
            }
        }
        return builder.build();
    }

    private NeuronLayerModel findLayerConnection() {
        return findLayer(layerId + 1, net);
    }

}
