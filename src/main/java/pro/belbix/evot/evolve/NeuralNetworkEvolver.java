package pro.belbix.evot.evolve;

import pro.belbix.evot.protobuf.neuron.*;

import java.util.ArrayList;
import java.util.List;

import static pro.belbix.evot.utils.NeuronUtils.defaultWeight;
import static pro.belbix.evot.utils.RandomUtils.getRandomElement;

public class NeuralNetworkEvolver {
    private final NeuralNetworkModel net;
    private final double evolveFactor;
    private final int maxInput;

    public NeuralNetworkEvolver(NeuralNetworkModel net, double evolveFactor, int maxInput) {
        this.net = net;
        this.evolveFactor = evolveFactor;
        this.maxInput = maxInput;
    }

    public NeuralNetworkModel evolve() {
        NeuralNetworkModel.Builder builder = NeuralNetworkModel.newBuilder();
        int maxId = Integer.MIN_VALUE;
        for (NeuronLayerModel layer : net.getLayersList()) {
            int layerId = layer.getId();
            if (layerId < 0) {
                builder.addLayers(layer);
                continue;
            }
            builder.addLayers(new LayerEvolver(net, layer.getId(), evolveFactor, maxInput).evolve());
            if (layerId > maxId) {
                maxId = layerId;
            }
        }

        //create new layer
//        NeuronLayerModel lastLayer = findLayer(maxId, net);
//        NeuronLayerModel newLayer = new LayerProducer(lastLayer, evolveFactor, maxInput).createLayer();
//        if (newLayer != null && lastLayer != null) {
//            builder.addLayers(newLayer);
//            builder.addLayers(createLinksBetweenLayers(newLayer, lastLayer));
//        }

        return builder.build();
    }

    private NeuronLayerModel createLinksBetweenLayers(NeuronLayerModel newLayer, NeuronLayerModel lastLayer) {
        List<NeuronModel> lastLayerNeuronsNew = new ArrayList<>();
        for (NeuronModel n : lastLayer.getNeuronsList()) {
            NeuronModel.Builder nB = n.toBuilder();
            NeuronModel newNeuron = getRandomElement(newLayer.getNeuronsList());
            nB.putLinks(newLayer.getId(),
                    Weight.newBuilder().putWeights(
                            newNeuron.getId(),
                            DoubleWithCounter.newBuilder().setValue(defaultWeight())
                                    .setPositive(newNeuron.getPositive()).build()
                    ).build()
            );
            lastLayerNeuronsNew.add(nB.build());
        }

        NeuronLayerModel.Builder updatedLastLayer = lastLayer.toBuilder();
        updatedLastLayer.clearNeurons();
        updatedLastLayer.addAllNeurons(lastLayerNeuronsNew);
        return updatedLastLayer.build();
    }


}
