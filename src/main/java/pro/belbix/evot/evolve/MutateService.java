package pro.belbix.evot.evolve;

import lombok.Data;
import pro.belbix.evot.protobuf.neuron.NeuronLayerModel;

import static pro.belbix.evot.utils.RandomUtils.*;

@Data
public class MutateService {
    private static boolean NORMAL_DISTRIBUTION = false;
    private final double factor;
    private final double mutateDoubleMax;
    private final double mutateDoubleBase;
    private final double baseChanceDouble;
    private final double baseChanceLink;
    private final double baseChanceNeuron;
    private final double baseChanceHidden;
    private final double countBase;
    private final double countMin;
    private final double neuronMutateFacrtorMax;

    public MutateService(NeuronLayerModel layer, double factor) {
        this.factor = factor;
        this.mutateDoubleMax = layer.getMutateDoubleMax();
        this.mutateDoubleBase = layer.getMutateDoubleBase();
        this.baseChanceDouble = layer.getBaseChanceDouble();
        this.baseChanceLink = layer.getBaseChanceLink();
        this.baseChanceNeuron = layer.getBaseChanceNeuron();
        this.baseChanceHidden = layer.getBaseChanceHidden();
        this.countBase = layer.getCountBase();
        this.countMin = layer.getCountMin();
        this.neuronMutateFacrtorMax = 10_000; //TODO
    }

    public double getMutateChange(double mutateFactor) {
        if (mutateDoubleBase >= mutateDoubleMax) {
            return 0;
        }

        double base = getRandomDouble();

        if (mutateFactor <= 1) {
            return base;
        }
        if (mutateFactor >= neuronMutateFacrtorMax) {
            return 0;
        }

        return base / Math.log(mutateFactor);
    }

    public static boolean isChance(double minChance) {
        double currentChance = random() * 100.0;
        return currentChance > 100 - minChance;
    }

    public double mutateDoubleReduce(double d, long epochCount) {
        if (!isChance(calcChance(epochCount, ChanceType.CHANCE_DOUBLE, true))) {
            return d;
        }
        return mutateDouble(d);
    }

    public double mutateDoubleIncrease(double d, long count) {
        if (!isChance(calcChance(count, ChanceType.CHANCE_DOUBLE, false))) {
            return d;
        }
        return mutateDouble(d);
    }

    private double mutateDouble(double d) {
        double mutate;
        if (NORMAL_DISTRIBUTION) {
            mutate = (normalDistributionRandom() / 5) * mutateDoubleBase * factor;
        } else {
            mutate = random() * mutateDoubleBase * factor;
            if (randomBinaryInt() == 0) {
                mutate = -mutate;
            }
        }
        d = d + mutate;
        if (d > mutateDoubleMax) {
            d = mutateDoubleMax;
        }
        if (d < -mutateDoubleMax) {
            d = -mutateDoubleMax;
        }
        return d;
    }

    public double getRandomDouble() {
        return random() * mutateDoubleBase * factor;
    }

    public double calcChance(double count, ChanceType chanceType, boolean reduce) {
        double chance;
        switch (chanceType) {
            case CHANCE_LINK:
                chance = baseChanceLink;
                break;
            case CHANCE_DOUBLE:
                chance = this.baseChanceDouble;
                break;
            case CHANCE_NEURON_CREATE:
                chance = baseChanceNeuron;
                break;
            case CHANCE_NEURON_DEATH:
                chance = baseChanceNeuron;
                break;
            case FIFTY_CHANCE:
                chance = 50.0;
                break;
            case CHANCE_LAYER:
                chance = baseChanceHidden;
                break;
            default:
                throw new IllegalStateException("Unknown chance type");
        }
        if (reduce) {
            double t = (countBase - count);
            if (t < countMin) {
                t = countMin;
            }
            return chance * (0.0 + ((t) / countBase));
        } else {
            double t = (countBase - count);
            if (t < countMin) {
                t = countMin;
            }
            return chance - (chance * (t / countBase));
        }
    }

    public void mutateLayersChance(NeuronLayerModel.Builder builder, NeuronLayerModel layer) {
        long epochCount = 0;
        builder.setMutateDoubleMax(mutateDoubleReduce(layer.getMutateDoubleMax(), epochCount));
        builder.setMutateDoubleBase(mutateDoubleReduce(layer.getMutateDoubleBase(), epochCount));
        builder.setBaseChanceDouble(mutateDoubleReduce(layer.getBaseChanceDouble(), epochCount));
        builder.setBaseChanceLink(mutateDoubleReduce(layer.getBaseChanceLink(), epochCount));
        builder.setBaseChanceNeuron(mutateDoubleReduce(layer.getBaseChanceNeuron(), epochCount));
        builder.setCountBase(mutateDoubleReduce(layer.getCountBase(), epochCount));
        builder.setCountMin(mutateDoubleReduce(layer.getCountMin(), epochCount));
    }

    public enum ChanceType {
        CHANCE_DOUBLE, CHANCE_LINK, CHANCE_NEURON_CREATE, CHANCE_NEURON_DEATH, CHANCE_LAYER, FIFTY_CHANCE
    }
}
