package pro.belbix.evot.evolve;

import pro.belbix.evot.protobuf.neuron.NeuralNetworkModel;
import pro.belbix.evot.protobuf.neuron.NeuronLayerModel;
import pro.belbix.evot.protobuf.neuron.NeuronModel;

public class NeuralNetworkEducator {
    private final NeuralNetworkModel net;
    private final double educateFactor;

    public NeuralNetworkEducator(NeuralNetworkModel net, double educateFactor) {
        this.net = net;
        this.educateFactor = educateFactor;
    }

    public NeuralNetworkModel educate() {
        NeuralNetworkModel.Builder builder = NeuralNetworkModel.newBuilder();
        for (NeuronLayerModel layer : net.getLayersList()) {
            int layerId = layer.getId();
            if (layerId < 0) {
                builder.addLayers(layer);
                continue;
            }
            builder.addLayers(educateLayer(layer));
        }
        return builder.build();
    }

    public NeuronLayerModel educateLayer(NeuronLayerModel layer) {
        NeuronLayerModel.Builder builder = layer.toBuilder();
        MutateService mutateService = new MutateService(layer, educateFactor);

        for (NeuronModel n : layer.getNeuronsList()) {
            builder.addNeurons(new NeuronEvolver(n, layer.getId(), net, mutateService, false).evolve());
        }
        return builder.build();
    }
}
