package pro.belbix.evot.evolve;

import pro.belbix.evot.protobuf.neuron.NeuronLayerModel;
import pro.belbix.evot.protobuf.neuron.NeuronModel;

import static pro.belbix.evot.evolve.MutateService.ChanceType.CHANCE_LAYER;
import static pro.belbix.evot.evolve.MutateService.isChance;

public class LayerProducer {
    private final NeuronLayerModel parentLayer;
    private final double evolveFactor;
    private final int maxInput;

    public LayerProducer(NeuronLayerModel parentLayer, double evolveFactor, int maxInput) {
        this.parentLayer = parentLayer;
        this.evolveFactor = evolveFactor;
        this.maxInput = maxInput;
    }

    public NeuronLayerModel createLayer() {
        MutateService mutateService = new MutateService(parentLayer, evolveFactor);
        if (!isChance(mutateService.calcChance(0, CHANCE_LAYER, true))) {
            return null;
        }

        NeuronLayerModel.Builder builder = NeuronLayerModel.newBuilder();
        builder.setId(parentLayer.getId() + 1);
        mutateService.mutateLayersChance(builder, parentLayer);

        NeuronModel newNeuron = NeuronProducer.createNeuron(builder.getNeuronsList(), null, mutateService, maxInput);
        builder.addNeurons(newNeuron);

        return builder.build();


    }

}
