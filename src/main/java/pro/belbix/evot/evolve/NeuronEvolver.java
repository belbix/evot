package pro.belbix.evot.evolve;

import pro.belbix.evot.protobuf.neuron.*;
import pro.belbix.evot.utils.NeuronUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import static pro.belbix.evot.evolve.MutateService.ChanceType.CHANCE_LINK;
import static pro.belbix.evot.evolve.MutateService.isChance;
import static pro.belbix.evot.utils.NeuronUtils.*;
import static pro.belbix.evot.utils.RandomUtils.getRandomElement;

public class NeuronEvolver {
    private final static boolean TRACE = false;
    private final NeuronModel neuron;
    private final int layerId;
    private final NeuralNetworkModel net;
    private final MutateService mutateService;
    private final boolean evolveStructure;
    private boolean changed = false;

    public NeuronEvolver(NeuronModel neuron,
                         int layerId,
                         NeuralNetworkModel net,
                         MutateService mutateService,
                         boolean evolveStructure) {
        this.neuron = neuron;
        this.layerId = layerId;
        this.net = net;
        this.mutateService = mutateService;
        this.evolveStructure = evolveStructure;
    }

    public NeuronModel evolve() {
        NeuronModel.Builder builder = neuron.toBuilder();
        updateThreshold(builder);
        updateWeights(builder);
        updateEpochCount(builder);
        if (evolveStructure) {
            tryToKillLink(builder);
            tryToCreateLink(builder);
        }
        return builder.build();
    }

    private void updateThreshold(NeuronModel.Builder builder) {
        double newThreshold = mutateService.mutateDoubleReduce(neuron.getThreshold(), neuron.getEpochCount());
        if (newThreshold <= 0) {
            newThreshold = 0.01;
        }
        builder.setThreshold(newThreshold);
        if (neuron.getThreshold() != newThreshold) {
            changed = true;
        }
    }

    private void updateWeights(NeuronModel.Builder builder) {
        AtomicBoolean changed = new AtomicBoolean(false);
        Map<Integer, Weight> newLinks = NeuronUtils.updateLinks(builder.getLinksMap(), p -> {
            DoubleWithCounter d = neuron.getLinksMap().get(p.getLayerId()).getWeightsOrDefault(p.getNeuronId(), null);
            if (d == null) {
                throw new IllegalStateException("Not found neuron with position " + p);
            }
            double newValue = mutateService.mutateDoubleReduce(d.getValue(), neuron.getEpochCount());
            if (d.getValue() != newValue) {
                changed.set(true);
            }
            return DoubleWithCounter.newBuilder()
                    .setValue(newValue)
                    .setCount(d.getCount())
                    .setPositive(d.getPositive())
                    .build();
        });
        builder.clearLinks().putAllLinks(newLinks);
        if (changed.get()) {
            this.changed = true;
        }
    }

    private void updateEpochCount(NeuronModel.Builder builder) {
        if (changed) {
            builder.setEpochCount(neuron.getEpochCount() + 1);
        } else {
            builder.setEpochCount(neuron.getEpochCount());
        }
    }

    private void tryToKillLink(NeuronModel.Builder builder) {
        if (neuron.getLinksMap().size() == 1) {
            return;
        }
        builder.clearLinks();
        for (Map.Entry<Integer, Weight> link : neuron.getLinksMap().entrySet()) {
            if (
//                    neuron.getCountOfSuccess() == 0 && //this is shortcut, need more analise
                    isChance(mutateService.calcChance(neuron.getActivatedCount(), CHANCE_LINK, true))) {
                changed = true;
                if (TRACE) System.out.println("Kill link " + link);
                continue;
            }
            builder.putLinks(link.getKey(), link.getValue());
        }
    }

    private void tryToCreateLink(NeuronModel.Builder builder) {
        if (!isChance(mutateService.calcChance(neuron.getEpochCount(), CHANCE_LINK, true))) {
            return;
        }
        addLink(builder);
        changed = true;
    }

    public void addLink(NeuronModel.Builder builder) {
        int nextLayerId = layerId + 1;
        NeuronLayerModel nextLayer = findLayer(nextLayerId, net);
        if (nextLayer == null) {
            nextLayerId = -1;
        }
        nextLayer = findLayer(nextLayerId, net);
        if (nextLayer == null) {
            return;
        }
        List<Integer> nextIds = new ArrayList<>();
        for (NeuronModel nextNeuron : nextLayer.getNeuronsList()) {
            nextIds.add(nextNeuron.getId());
        }

        if (nextIds.isEmpty()) {
            throw new IllegalStateException("layer with id " + nextLayerId + " doesn't have any neurons");
        }
        Integer nextId = getRandomElement(nextIds);
        NeuronModel nextNeuron = findNeuronInLayer(nextLayer, nextId);
        builder.putLinks(nextLayerId,
                Weight.newBuilder().putWeights(
                        nextId,
                        DoubleWithCounter.newBuilder().setValue(defaultWeight())
                                .setPositive(nextNeuron.getPositive()).build()
                ).build()
        );
    }
}
